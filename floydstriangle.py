__author__ = 'treed2017'
n = int(input('Enter an amount of rows '))

row = 1
col = 1
num = 1

triangle = ''

rowTotal = 0

while row <= n:
    while col <= row:
        triangle += ' ' + str(num)
        rowTotal += num
        num += 1
        col += 1
    triangle += ' ' + ' Total = ' + str(rowTotal) + '\n'
    rowTotal = 0
    row += 1
    col = 1
print(triangle)